#!/usr/bin/env python3
from telegram import Update
from telegram.ext import CallbackContext

def give_credits(update: Update, context: CallbackContext):
    context.bot.sendMessage(chat_id=update.message.chat_id,
                    text="Je suis open source, viens m'inspecter ici : https://gitlab.com/metaheavens/anastasia")
