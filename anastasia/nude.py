import urllib.request
import json
from bs4 import BeautifulSoup
import datetime
import random
import io
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup, InputFile, InputMediaPhoto
from telegram.ext import CallbackContext
from anastasia.database import *

def get_nude(update: Update, context: CallbackContext):
    days_before = random.randint(0, 200)
    day = (datetime.datetime.now() - datetime.timedelta(days_before))
    while day.weekday() == 6 or day.weekday() == 5:
        days_before = random.randint(0, 200)
        day = (datetime.datetime.now() - datetime.timedelta(days_before))
    day_str = day.strftime("%Y/%m/%d")
    url = "https://www.bonjourmadame.fr/" + str(day_str)
    print(url)
    site = urllib.request.urlopen(url)
    html = site.read().decode('iso-8859-1')
    soup = BeautifulSoup(html, 'html.parser')

    nude_elem = soup.find("div", attrs={"class": "post-content"})
    nude = nude_elem.p.img['src']
    image_blob = urllib.request.urlopen(nude).read()
    nude_image = NudeImage.replace(image = image_blob, url = nude).execute()
    MessageIdToNudeImage(
        nude = nude_image,
        message_id = update.message.message_id,
        chat_id = update.message.chat_id
    ).save(force_insert=True)
    print(update)
    print("Message ID: " + str(update.message.message_id))
    image = io.BytesIO(image_blob)

    button_list = [InlineKeyboardButton(str(x), callback_data=json.dumps({"value": str(x), "message_id": update.message.message_id})) for x in range(1, 6)]
    reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=5))

    context.bot.sendPhoto(chat_id=update.message.chat_id, photo=image, reply_markup=reply_markup)

def nude_callback(update: Update, context: CallbackContext):
    print(update.callback_query.data)
    data = json.loads(update.callback_query.data)
    print(data)
    note = NudeNote.replace(
        user_id = update.callback_query.from_user.id,
        chat_id = update.callback_query.chat_instance,
        message_id = data["message_id"],
        note = int(data["value"])
    ).execute()
    print(update)
    print("Message ID: " + str(update.callback_query.message.message_id))
    update.callback_query.answer("You've noted this image " + str(data["value"]) + ". You can still change your vote.")

def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, [header_buttons])
    if footer_buttons:
        menu.append([footer_buttons])
    return menu

def get_nude_scoreboard(update: Update, context: CallbackContext):
    query = (NudeImage.select()
            .join(MessageIdToNudeImage)
            .join(NudeNote, on=(( NudeNote.message_id == MessageIdToNudeImage.message_id ) & ( NudeNote.message_id == MessageIdToNudeImage.message_id )))
            .group_by(NudeImage.id)
            .order_by(fn.AVG(NudeNote.note).desc())
            .limit(5)
            )

    photos = [InputMediaPhoto(io.BytesIO(result.image)) for result in query]

    context.bot.sendMediaGroup(chat_id=update.message.chat_id, media=photos)
