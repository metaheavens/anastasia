from peewee import *
from anastasia import confighelper
import sys

conf = confighelper.ConfigHelper(sys.argv[1])
db = SqliteDatabase(conf.get_sqllite_file())
db.connect()

class BaseModel(Model):
    class Meta:
        database = db

class NudeNote(BaseModel):
    user_id = CharField()
    chat_id = CharField()
    message_id = CharField()
    note = IntegerField()

    class Meta:
        primary_key = CompositeKey("user_id", "chat_id", "message_id")

class NudeImage(BaseModel):
    image = BlobField()
    url = TextField(unique = True)

class MessageIdToNudeImage(BaseModel):
    nude = ForeignKeyField(NudeImage)
    message_id = CharField()
    chat_id = CharField()
    class Meta:
        primary_key = CompositeKey("nude", "message_id", "chat_id")

db.create_tables([NudeNote, NudeImage, MessageIdToNudeImage])
